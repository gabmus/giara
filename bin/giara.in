#!@PYTHON@

import os
import sys
import signal
import gettext
import locale


sys.path.insert(1, '@pythondir@')

VERSION = '@VERSION@'
pkgdatadir = '@pkgdatadir@'
localedir = '@localedir@'
builddir = os.environ.get('MESON_BUILD_ROOT')
if builddir:
    pkgdatadir = os.path.join(builddir, 'data')
    os.environ['GSETTINGS_SCHEMA_DIR'] = pkgdatadir
    sys.dont_write_bytecode = True
    sys.path.insert(1, os.environ['MESON_SOURCE_ROOT'])

signal.signal(signal.SIGINT, signal.SIG_DFL)

# Why both locale and gettext?
# gettext works for the python part
# but not for the glade/xml files
# they need locale
# don't ask me, it's effin weird
# I copied this from uberwriter
try:
    locale.bindtextdomain('@projectname@', localedir)
    locale.textdomain('@projectname@')
except AttributeError as e:
    # Python built without gettext support doesn't have bindtextdomain()
    # and textdomain()
    print("Couldn't bind the gettext translation domain. Some translations"
    " won't work. Error: \n{}".format(e))
gettext.textdomain('@projectname@')
gettext.bindtextdomain('@projectname@', localedir)

if __name__ == '__main__':
    # os.environ['G_MESSAGES_DEBUG'] = '0'

    import gi

    gi.require_version('Gtk', '4.0')
    gi.require_version('Gdk', '4.0')
    gi.require_version('Adw', '1')
    gi.require_version('GtkSource', '5')

    from gi.repository import Gio, GObject, GtkSource

    # line below and necessary imports above required to accept GtkSourceView
    # from glade file
    GObject.type_register(GtkSource.View)

    resource = Gio.Resource.load(os.path.join(pkgdatadir, '@app_id@.gresource'))
    resource._register()

    from @projectname@ import constants

    constants.APP_ID = '@app_id@'
    constants.APP_NAME = '@prettyname@'
    constants.RESOURCE_PREFIX = '/@app_id_aspath@'
    constants.PROFILE = '@profile@'

    from @projectname@ import __main__
    __main__.main()
