from urllib.parse import urlparse
from os.path import basename, splitext
from hashlib import sha256


def get_url_filename(link: str) -> str:
    return basename(urlparse(link).path)


IMAGE_EXTENSIONS = [
    'jpg', 'jpeg', 'png', 'bmp', 'tiff', 'svg', 'gif'
]
VIDEO_EXTENSIONS = [
    'mp4', 'webm'
]


def get_file_extension(path: str) -> str:
    return splitext(path)[-1].strip('.').split('?')[0].lower()


def __has_extensions(path: str, extensions: list) -> bool:
    if path is None:
        return False
    return get_file_extension(path) in extensions


def is_image(path: str) -> bool:
    return __has_extensions(path, IMAGE_EXTENSIONS)


def is_gif(path: str) -> bool:
    return __has_extensions(path, ['gif'])


def is_video(path: str) -> bool:
    return __has_extensions(path, VIDEO_EXTENSIONS)


def is_media(path: str) -> bool:
    return __has_extensions(path, IMAGE_EXTENSIONS+VIDEO_EXTENSIONS)


def sha256sum(txt: str) -> str:
    return sha256(txt.encode()).hexdigest()
