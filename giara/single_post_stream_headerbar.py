from giara.constants import RESOURCE_PREFIX
from gettext import gettext as _
from gi.repository import Gtk
from giara.squeezing_viewswitcher_headerbar import \
    SqueezingViewSwitcherHeaderbar
from giara.ellipsized_label import make_ellipsized_label


class SinglePostStreamHeaderbar(SqueezingViewSwitcherHeaderbar):
    def __init__(self, title, stack=None, **kwargs):
        super().__init__(
            Gtk.Builder.new_from_resource(
                f'{RESOURCE_PREFIX}/ui/post_details_headerbar.ui'
            ),
            stack,
            view_switcher=False,
            sort_menu=stack is not None,
            sorting_methods={
                'hot': {
                    'name': _('Hot'),
                    'icon': 'hot-symbolic'
                },
                'new': {
                    'name': _('New'),
                    'icon': 'new-symbolic'
                },
                'top': {
                    'name': _('Top'),
                    'icon': 'arrow1-up-symbolic'
                },
                'rising': {
                    'name': _('Rising'),
                    'icon': 'rising-symbolic'
                },
                'controversial': {
                    'name': _('Controversial'),
                    'icon': 'controversial-symbolic'
                }
            },
            **kwargs
        )
        self.title = title
        self.headerbar.set_title_widget(make_ellipsized_label(self.title))
        self.back_btn = self.builder.get_object('back_btn')
        self.refresh_btn = self.builder.get_object('refresh_btn')
