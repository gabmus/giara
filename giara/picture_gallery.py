from gettext import gettext as _
from gi.repository import Gtk, Adw
from giara.picture_view import PictureView
from giara.confManager import ConfManager
from giara.common_functions import show_hide_gallery_btns


class PictureGallery(Gtk.Box):
    def __init__(self, pictures: list, open_media_func=None, blur=False):
        super().__init__(orientation=Gtk.Orientation.VERTICAL)
        self.open_media_func = open_media_func
        self.confman = ConfManager()
        self.set_size_request(-1, self.confman.conf['max_picture_height'])

        self.overlay = Gtk.Overlay(vexpand=True, hexpand=True)
        self.carousel = Adw.Carousel(
            spacing=12, vexpand=True, allow_scroll_wheel=False
        )
        self.carousel.connect(
            'page-changed', lambda *args: self.show_hide_gallery_btns()
        )
        self.overlay.set_child(self.carousel)
        self.prev_btn = Gtk.Button(
            icon_name='go-previous-symbolic',
            halign=Gtk.Align.START, valign=Gtk.Align.CENTER,
            hexpand=False, vexpand=False,
            margin_start=6, tooltip_text=_('Previous picture')
        )
        self.next_btn = Gtk.Button(
            icon_name='go-next-symbolic',
            halign=Gtk.Align.END, valign=Gtk.Align.CENTER,
            hexpand=False, vexpand=False,
            margin_end=6, tooltip_text=_('Next picture')
        )
        self.prev_btn.connect('clicked', self.go_prev)
        self.next_btn.connect('clicked', self.go_next)
        for btn in (self.prev_btn, self.next_btn):
            for c in ('circular', 'gallery_btn'):
                btn.get_style_context().add_class(c)
            self.overlay.add_overlay(btn)
        self.dots = Adw.CarouselIndicatorDots(carousel=self.carousel)

        self.append(self.overlay)
        self.append(self.dots)

        self.children = list()
        self.set_pictures(pictures, blur=blur)

    def can_navigate(self):
        pos = self.carousel.get_position()
        return pos - int(pos) <= 0

    def go_prev(self, *args):
        if self.can_navigate():
            pos = self.carousel.get_position()
            if pos <= 0:
                return
            self.carousel.scroll_to(self.children[int(pos)-1], True)
            self.show_hide_gallery_btns(int(self.carousel.get_position()) - 1)

    def go_next(self, *args):
        if self.can_navigate():
            pos = self.carousel.get_position()
            if pos+1 >= len(self.children):
                return
            self.carousel.scroll_to(self.children[int(pos)+1], True)
            self.show_hide_gallery_btns(int(self.carousel.get_position()) + 1)

    def empty(self):
        for child in self.children:
            self.carousel.remove(child)
        self.children = list()

    def set_pictures(self, pictures, blur=False):
        self.pictures = pictures
        self.empty()
        for picture in self.pictures:
            pic_view = PictureView(
                picture, False, self.open_media_func, blur=blur
            )
            self.children.append(pic_view)
            self.carousel.append(pic_view)
        self.show_hide_gallery_btns()

    def show_hide_gallery_btns(self, pos=None):
        if pos is None:
            pos = int(self.carousel.get_position())
        show_hide_gallery_btns(
            pos,
            len(self.pictures),
            self.prev_btn,
            self.next_btn
        )

    def get_current_picture_path(self):
        pos = int(self.carousel.get_position())
        return self.pictures[pos]
